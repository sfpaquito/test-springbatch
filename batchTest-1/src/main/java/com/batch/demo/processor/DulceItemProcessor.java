package com.batch.demo.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.batch.demo.model.Dulces;

public class DulceItemProcessor implements ItemProcessor<Dulces, Dulces>{
	
	private static final Logger LOG = LoggerFactory.getLogger(DulceItemProcessor.class);

	@Override
	public Dulces process(Dulces item) throws Exception {
		String nombre = item.getNombre().toUpperCase();
		String sabor = item.getSabor().toUpperCase();
		String cantidad = item.getCantidad().toUpperCase();
		
		Dulces dulces = new Dulces(nombre, sabor, cantidad); 
		
		LOG.info("Convirtiendo de: ("+item+") en ("+dulces+")");
		
		return null;
	}

}
