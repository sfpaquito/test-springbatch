package com.batch.demo.model;

public class Dulces {
	
	private String nombre;
	private String  sabor;
	private String  cantidad;
	
	public Dulces() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getSabor() {
		return sabor;
	}

	public void setSabor(String sabor) {
		this.sabor = sabor;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}

	public Dulces(String nombre, String sabor, String cantidad) {
		super();
		this.nombre = nombre;
		this.sabor = sabor;
		this.cantidad = cantidad;
	}

	@Override
	public String toString() {
		return "Dulces [nombre=" + nombre + ", sabor=" + sabor + ", cantidad=" + cantidad + "]";
	}
}
