package com.batch.demo;


import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import com.batch.demo.listener.JobListener;
import com.batch.demo.model.Dulces;
import com.batch.demo.processor.DulceItemProcessor;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

	@Autowired
	public JobBuilderFactory builderFactory;
	
	@Autowired
	public StepBuilderFactory stepBuilderFactory;
	
	@Bean
	public FlatFileItemReader<Dulces> reader(){
		return new FlatFileItemReaderBuilder()
				.name("DulceItemReader")
				.resource(new ClassPathResource("Sample-data.csv"))
				.delimited()
				.names(new String[] {"nombre","sabor","cantidad"})
				.fieldSetMapper(new BeanWrapperFieldSetMapper<Dulces>() {{
					setTargetType(Dulces.class);
				}})
		       .build();
				}
	
	public DulceItemProcessor dulceItemProcessor() {
		return new DulceItemProcessor();		
	}
	
	@Bean
	public JdbcBatchItemWriter<Dulces> writer(javax.sql.DataSource dataSource){
		return new JdbcBatchItemWriterBuilder<Dulces>()
				   .itemSqlParameterSourceProvider(new  BeanPropertyItemSqlParameterSourceProvider<>())
				   .sql("INSERT INTO dulces ( nombre, sabor, cantidad) VALUES (:nombre, :sabor, :cantidad)")
				   .dataSource(dataSource)
				   .build();
		
	}
	
	@Bean
	public Job importDulcesJob(JobListener jobListener, Step step) {
		return builderFactory.get("importDulcesJob")
			   .incrementer(new RunIdIncrementer())
			   .listener(jobListener)
			   .flow(step)
			   .end()
			   .build();
		
	}
	
	@Bean
	public Step step(JdbcBatchItemWriter<Dulces>writer){
		return stepBuilderFactory.get("step")
			   .<Dulces, Dulces> chunk(10)
			   .reader(reader())
//			   .processor(dulceItemProcessor())
			   .writer(writer)
			   .build();
}
}
	
 